import React from "react";
import { Card, CardBody } from "@nextui-org/react";
import clsx from "clsx";

interface Props {
    className?: string;
    title: string;
    content: string | JSX.Element;
    illustration?: JSX.Element;
    image?: JSX.Element;
    inverted?: boolean;
    titleOutside?: boolean;
}

const Textbox: React.FC<Props> = (props: Props) => {
    const title: JSX.Element = (
        <h3 className="mb-4 text-center text-2xl font-bold text-sky-700">
            {props.title}
        </h3>
    );

    return (
        <div className="max-w-[1200px] m-auto p-4">
            {props.titleOutside && title}
            <div
                className={clsx(
                    "flex items-center justify-evenly gap-4",
                    props.inverted && " flex-row-reverse",
                    props.className && props.className
                )}
            >
                <Card
                    className={
                        "bg-gray-100 p-10 max-[1100px]:max-w-full" +
                        (props.illustration || props.image
                            ? " max-w-[700px]"
                            : " max-w-full")
                    }
                    shadow="none"
                >
                    <CardBody>
                        {!props.titleOutside && title}
                        {typeof props.content === "string" ? (
                            <p className="leading-loose">{props.content}</p>
                        ) : (
                            props.content
                        )}
                    </CardBody>
                </Card>

                {props.illustration && (
                    <div className=" mx-4 max-[1100px]:hidden [&>svg]:h-auto [&>svg]:w-[500px]">
                        {props.illustration}
                    </div>
                )}

                {props.image}
            </div>
        </div>
    );
};

export default Textbox;
