import React from "react";
import Link from "next/link";
import { Snippet } from "@nextui-org/react";
import { scrollToTop } from "../utils/scroll";
import { useWindowSize } from "@uidotdev/usehooks";

const Impressum: React.FC = () => {
    const { width } = useWindowSize();

    return (
        <footer
            id="impressum"
            className="flex flex-wrap gap-4 justify-between w-full px-4 pb-4 pt-12 max-w-[1200px] m-auto">
            <div className="flex flex-col gap-4">
                <p><strong>Impressum</strong></p>
                <p>Marc Mäurer</p>
                <p>Hessen, 63505 Langenselbold, Rödelbergstraße&nbsp;1</p>
            </div>

            <div className="flex flex-col gap-4">
                {width! >= 350 ?
                 (
                     <React.Fragment>
                        <Snippet symbol={<strong>Email </strong>}>
                            {"sprachschule.maeurer@pm.me"}
                        </Snippet>
                        <Snippet symbol={<strong>Handy </strong>}>
                            015234794964
                        </Snippet>
                     </React.Fragment>
                 ) : (
                     <React.Fragment>
                        <p className="break-all">{"sprachschule.maeurer@pm.me"}</p>
                        <p className="break-all">01523 4794964</p>
                     </React.Fragment>
                )}

                <Link
                    href="/datenschutz"
                    onClick={scrollToTop}
                    className="underline">
                    Datenschutz
                </Link>
            </div>
        </footer>
    );
};

export default Impressum;
