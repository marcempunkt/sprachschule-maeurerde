import React from "react";

import Nav from "../Nav";
import DataProtectionStatement from "../DataProtectionStatement";

const DataProtectionStatementPage: React.FC = () => {
    return (
        <React.Fragment>
            <Nav isDataProtectionStatementPage={true} />
            <DataProtectionStatement />
        </React.Fragment>
    );
};

export default DataProtectionStatementPage;
