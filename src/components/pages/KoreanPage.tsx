import React from "react";

import Nav from "../Nav";
import Landing from "../Landing";
import ChooseLanguage from "../ChooseLanguage";
import CommingSoon from "../CommingSoon";

const KoreanPage: React.FC = () => {
    return (
        <React.Fragment>
            <Nav />
            <Landing />
            <ChooseLanguage />
            <CommingSoon />
        </React.Fragment>
    );
};

export default KoreanPage;
