import React, { ReactNode } from "react";
import { Button } from "@nextui-org/react";
import Link from "next/link";

export default function PriceCard(props: {
    name: string;
    id: string;
    featured: boolean;
    price: number;
    /* hourPrice?: number; */
    icon: ReactNode;
    description?: string;
}) {
    function classNames(...classes: Array<string>) {
        return classes.filter(Boolean).join(" ");
    }

    function hourRate(priceFor90min: number): number {
        if (priceFor90min === 0) return 0;
        return Math.ceil((priceFor90min / 3) * 2) - 0.01;
    }

    return (
        <div
            className={classNames(
                props.featured
                    ? "bg-gray-900 ring-gray-900 max-[564px]:order-first"
                    : "ring-gray-200",
                "flex flex-col gap-4 items-center justify-between rounded-3xl p-4 ring-1 w-[250px]"
            )}
        >
            <h2
                id={props.id}
                className={classNames(
                    props.featured ? "text-white" : "text-gray-900",
                    "text-xl font-semibold leading-loose"
                )}
            >
                {props.name}
            </h2>

            <div
                className={classNames(
                    props.featured
                        ? "[&>svg]:stroke-white"
                        : "[&>svg]:stroke-gray-900",
                    "[&>svg]:w-[100px] [&>svg]:h-[100px]"
                )}>
                {props.icon}
            </div>

            {props.price > 0 && (
                <div className="flex flex-col items-center gap-1">
                    <p className="flex items-baseline">
                        <span className="text-gray-900 text-3xl font-bold tracking-tight">
                            {hourRate(props.price)}€
                        </span>
                        <span className={"text-gray-600 text-base font-semibold leading-loose ml-2"}>
                            /1h
                        </span>
                    </p>

                    <p className="flex items-baseline text-gray-400">
                        <span className="pr-1">
                            oder
                        </span>
                        <span className={"tracking-tight"}>
                            {props.price}€
                        </span>
                        <span className={"text-sm leading-loose ml-2"}>
                            /1h 30min
                        </span>
                    </p>
                </div>
            )}

            {props.price === 0 && (
                <p className="flex items-baseline">
                    <span className={"text-white text-4xl font-bold tracking-tight"}>
                        {props.price}€
                    </span>
                    <span
                        className={classNames(
                            props.featured
                                ? "text-gray-300"
                                : "text-gray-600",
                            "text-base font-semibold leading-loose ml-2"
                        )}>
                        /1h 30min
                    </span>
                </p>
            )}


            {props.description && (
                <p className="text-sm leading-loose text-gray-600">
                    {props.description}
                </p>
            )}

            {props.featured && (
                <Button
                    as={Link}
                    href="/#contact"
                    aria-describedby={props.id}
                    color="secondary"
                    className="bg-indigo-500 text-white font-bold w-full"
                >
                    Jetzt anfangen!
                </Button>
            )}
        </div>
    );
};
