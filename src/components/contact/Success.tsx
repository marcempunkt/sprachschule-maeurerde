import React from "react";
import { CheckCircle } from "react-feather";
import { Card } from "@nextui-org/react";

export default function Success(props: { message: string }) {
    return (
        <Card
            shadow="none"
            className="flex flex-row gap-4 p-4 items-center bg-green-50 border-green-200 w-full"
        >
            <CheckCircle
                className="h-5 w-5 text-green-400"
                aria-hidden="true"
            />
            <p className="font-medium text-green-800">{props.message}</p>
        </Card>
    );
};
