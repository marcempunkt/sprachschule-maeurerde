import React from "react";
import { XCircle } from "react-feather";
import { Card } from "@nextui-org/react";

export default function Error(props: { message: string }) {
    return (
        <Card
            shadow="none"
            className="flex flex-row gap-4 p-4 items-center bg-red-50 border-red-200 w-full"
        >
            <XCircle
                className="h-5 w-5 text-red-400"
                aria-hidden="true"
            />
            <p className="font-medium text-red-800">{props.message}</p>
        </Card>
    );
};
