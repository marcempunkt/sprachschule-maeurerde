import React, { useState, ChangeEvent } from "react";
import { Button, Card, Input, Select, SelectItem, Textarea } from "@nextui-org/react";
import Plunk from "@plunk/node";
import { render } from "@react-email/render";
import ConfirmationMail from "../../emails/ConfirmationMail";
import RequestMail from "../../emails/RequestMail";
import Error from "./Error";
import Success from "./Success";

export default function Contact() {
    type ClassType = "Gratisstunde" | "Präsenzunterricht" | "Online";

    const [name, setName] = useState<string>("");
    const [email, setEmail] = useState<string>("");
    const [message, setMessage] = useState<string>("");
    const [classType, setClassType] = useState<ClassType>("Gratisstunde");
    const [error, setError] = useState<string>("");
    const [success, setSuccess] = useState<string>("");
    const [sending, setSending] = useState<boolean>(false);
    const [emailsSent, setEmailsSent] = useState<number>(0);

    const plunk = new Plunk(process.env.NEXT_PUBLIC_EMAIL_API_KEY!);

    function resetForm() {
        setName("");
        setEmail("");
        setMessage("");
        setClassType("Gratisstunde");
        setSending(false);
    }

    async function sendRequestMail(email: string) {
        return await plunk.emails.send({
            to: "sprachschule.maeurer@pm.me",
            subject: "Anfrage für Japanisch Unterricht",
            body: render(
                <RequestMail
                    name={name}
                    email={email}
                    message={message}
                    classType={classType}
                />
            ),
        });
    }

    async function sendConfirmationMail(email: string) {
        return await plunk.emails.send({
            to: email,
            subject: "Bestätigungsanfrage für Japanisch Unterricht",
            body: render(<ConfirmationMail />),
        });
    }

    async function handleSubmit(e: React.FormEvent) {
        e.preventDefault();
        const toManyMailsSent: boolean = emailsSent >= 2;
        if (!email || toManyMailsSent) setSuccess("");
        if (toManyMailsSent)
            return setError(
                "Zu viele Emails wurden verschickt. Probiere es später noch einmal."
            );
        if (!email) return setError("Bitte gebe eine Email an.");

        try {
            setSending(true);
            await sendRequestMail(email);
            await sendConfirmationMail(email);
            setSuccess("Email wurde versendet. Eine Bestätigungsemail wird dir zugeschickt.");
            setError("");
            resetForm();
            setEmailsSent(emailsSent + 1);
        } catch (err: unknown) {
            setSuccess("");
            setError(
                "Bitte versuche es später noch einmal oder " +
                    "schreibe mir direkt eine Email an marc.maeurer@pm.me"
            );
        } finally {
            setSending(false);
        }
    }

    return (
        <div id="contact" className="px-4 py-20">
            <Card className="p-4 max-w-4xl m-auto">
                <form
                    className="flex flex-col gap-4"
                    onSubmit={handleSubmit}
                >
                    <h1 className="mb-4 text-center text-2xl font-bold text-sky-700">
                        Kontaktanfrage
                    </h1>

                    <Input
                        type="text"
                        label="Name"
                        onChange={(e) => setName(e.currentTarget.value)}
                        value={name}
                    />

                    <Input
                        type="email"
                        label="Email"
                        onChange={(e) => setEmail(e.currentTarget.value)}
                        value={email}
                    />

                    <Select
                        label="Unterrichtsart"
                        defaultSelectedKeys={["Gratisstunde"]}
                        onChange={(e) => setClassType(e.target.value as ClassType)}
                    >
                        <SelectItem key="Gratisstunde">{"Gratisstunde"}</SelectItem>
                        <SelectItem key="Präsenzunterricht">{"Präsenzunterricht"}</SelectItem>
                        <SelectItem key="Online">{"Online"}</SelectItem>
                    </Select>

                    <Textarea
                        label="Nachricht"
                        placeholder="Hast du noch Fragen zum Unterricht?"
                        value={message}
                        onChange={(e) => setMessage(e.currentTarget.value)}
                    />

                    {error && <Error message={error} />}
                    {success && <Success message={success} />}

                    <Button
                        isLoading={sending}
                        color="primary"
                        radius="md"
                        type="submit"
                    >
                        Email senden
                    </Button>
                </form>
            </Card>
        </div>
    );
};
