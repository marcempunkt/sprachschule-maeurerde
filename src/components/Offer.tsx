import React from "react";
import { Users, Gift, Monitor } from "react-feather";
import PriceCard from "./PriceCard";

const Offer: React.FC = () => {
    const tiers = [
        {
            name: "Online",
            id: "online",
            featured: false,
            price: 39.99,
            /* hourPrice: 26.99, */
            icon: <Monitor />,
            description: "pro Person",
        },
        {
            name: "Gratisstunde",
            id: "free",
            featured: true,
            price: 0,
            icon: <Gift />,
        },
        {
            name: "Präsenz",
            id: "private",
            featured: false,
            price: 44.99,
            /* hourPrice: 31.99, */
            icon: <Users />,
            description: "pro Person",
        },
    ];

    return (
        <>
            <div
                id="offer"
                className="isolate mx-auto my-10 flex w-full flex-wrap justify-center gap-8">
                {tiers.map((tier) => <PriceCard key={tier.id} {...tier} />)}
            </div>

            <p className="text-sm text-gray-500 text-center">
                Die Unterrichtsdauer kann nach Belieben angepasst werden.
            </p>
        </>
    );
};

export default Offer;
