import React, { useState, useEffect } from "react";
import Link from "next/link";
import { Tabs, Tab } from "@nextui-org/react";
import JapanFlag from "../assets/flags/japan.svg";
import KoreaFlag from "../assets/flags/korea.svg";
import { useWindowSize } from "@uidotdev/usehooks";

export default function ChooseLanguage() {
    type Language = "japanisch" | "koreanisch";

    const languages: Array<{ lang: Language; flag: JSX.Element }> = [
        {
            lang: "japanisch",
            flag: <JapanFlag />,
        },
        {
            lang: "koreanisch",
            flag: <KoreaFlag />,
        },
    ];

    const [language, setLanguage] = useState<Language>("japanisch");
    const { width } = useWindowSize();

    function capitalize(str: string) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    useEffect(() => {
        switch (window.location.pathname) {
            case "/japanisch":
                setLanguage("japanisch");
                break;
            case "/koreanisch":
                setLanguage("koreanisch");
                break;
        }
    }, []);

    return (
        <div className="flex flex-col gap-4 justify-center items-center">
            <h1 className="text-center text-3xl font-bold leading-loose text-sky-700">
                Welche Sprache möchtest Du lernen?
            </h1>

            <Tabs
                aria-label="Options"
                color="primary"
                size="lg"
                selectedKey={language}
                isVertical={width! <= 350}
            >
                {languages.map((l, i) => (
                    <Tab
                        key={l.lang}
                        title={
                            <Link key={i} href={`/${l.lang}`} onClick={() => setLanguage(l.lang)}>
                                <div className="flex gap-2 items-center [&>svg]:h-[2rem]">
                                    {l.flag}
                                    <span className="text-xl">{capitalize(l.lang)}</span>
                                </div>
                            </Link>
                        }
                    />
                ))}
            </Tabs>
        </div>
    );
};
