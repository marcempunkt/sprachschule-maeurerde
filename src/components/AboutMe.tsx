import React, { useEffect, useState } from "react";
import Image from "next/image";
import Textbox from "./Textbox";
import Me from "../assets/img/marc.jpg";

const AboutMe: React.FC = () => {
    const [years, setYears] = useState<number>(6);

    useEffect(() => {
        "use client";
        let y: number = Math.round(
            (new Date().valueOf() - new Date("2018-08-01").valueOf()) /
            1000 /
            60 /
            60 /
            24 /
            365.25
        );

        if (y < 5 || y > 15) y = 10;

        setYears(y);
    }, []);

    return (
        <div
            id="aboutme"
            className="my-12">
            <Textbox
                className="max-[768px]:flex-col-reverse w-full"
                title="Hallo! Ich heiße Marc."
                content={
                    <div>
                        <p className="leading-loose">
                                      Wenn Du nach Japanisch Nachhilfe oder Unterricht
                                      suchst, der auf Dich individuell angepasst ist, dann
                                      bist Du bei meinem Sprachkurs genau richtig. Ich
                                      unterrichte Japanisch schon seit {years} Jahren
                                      mit über 20 Schülern.
                                      Viele meiner Schüler können schon fließend auf
                                      Japanisch Gespräche führen. Dadurch dass ich kein
                                      Muttersprachler bin, aber fließend in Japanisch
                                      bin, habe ich ein tiefes Verständnis der Sprache
                                      und der Grammatik wie es kaum ein anderer
                                      Muttersprachler hat.
                        </p>
                        <p className="mt-4 text-center">
                            Noch nicht überzeugt? Dann probiere doch eine
                            <b>
                                <a href="#contact"> Gratisstunde </a>
                            </b>
                            aus!
                        </p>
                    </div>
                }
                image={
                    <Image
                        className="max-h-[300px] max-w-[300px] rounded-full w-full"
                        src={Me}
                        alt="Marc Mäurer"
                    />
                }
                inverted={true}
            />
        </div>
    );
};

export default AboutMe;
