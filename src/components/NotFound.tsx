import React from "react";

const NotFound: React.FC = () => {
    return <h1>404 - Not Found Go back or go to language buttons</h1>;
};

export default NotFound;
