import React from "react";
import { Card, CardBody } from "@nextui-org/react";

const CommingSoon: React.FC = () => {
    return (
        <div className="px-4 py-12 flex justify-center items-center w-full">
            <Card className="max-w-[800px] bg-red-200" shadow="none">
                <CardBody className="flex items-center font-bold w-full">
                    Bald verfügbar
                </CardBody>
            </Card>
        </div>
    );
};

export default CommingSoon;
