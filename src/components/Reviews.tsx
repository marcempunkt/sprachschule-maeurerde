import React, { useEffect, useRef } from "react";
import Star from "../assets/reviews/star.svg";
import { ScrollShadow } from "@nextui-org/react";
import { ExternalLink } from "react-feather";
import { animate } from "framer-motion";

export default function Reviews() {
    const controls = useRef<any>(null);

    const reviews = [
        {
            username: "Emilie",
            stars: 5,
            message: <>
                <p>
                    Auf der Suche nach einem Japanisch Lehrer bin ich online auf Marc gestoßen.
                    Ich wollte schon länger japanisch Unterricht nehmen und der Anstoß bei mir
                    war eine in 3 Monaten anstehende Reise nach Japan.
                    Total unkompliziert hat Marc mit mir den Unterricht begonnen und sich auf
                    meine Bedürfnisse eingestellt (vor allem auch um als „Touri“ etwas japanisch
                    nutzen zu können). Ich bin jetzt seit über einem halben Jahr bei Marc und
                    bin absolut glücklich. Marcs Unterricht ist strukturiert, aber auch individuell.
                    Mein Japanisch hat sich signifikant verbessert.
                    Ich kann den Unterricht wärmstens empfehlen!
                </p>
            </>,
            link: "https://maps.app.goo.gl/oP9UqavatkbGqy8Y8",
        },
        {
            username: "Sven",
            stars: 5,
            message: <>
                <p>
                    Ich lerne derzeit Japanisch bei der Sprachschule Mäurer und
                    kann nur Positives berichten. Der Lehrer ist äußerst freundlich und
                    schafft eine angenehme Lernatmosphäre, in der man sich wohlfühlt und
                    motiviert wird. Besonders schätze ich, dass der Unterricht
                    individuell an mein Lerntempo angepasst wird. Dadurch habe ich
                    nie das Gefühl, überfordert zu sein oder hinterherzuhinken.
                </p>
                <p>
                    Die Lehrmaterialien sind sehr gut ausgewählt und
                    unterstützen den Lernprozess optimal.
                    Ein Highlight ist die spürbare Freude des Lehrers am Unterricht.
                    Seine Begeisterung ist ansteckend und macht jede Unterrichtsstunde
                    zu einem Vergnügen.
                </p>
                <p>
                    Zudem hat Herr Mäurer spezielle Lernhilfen für mich
                    individuell erstellt, die auf Reisen ausgerichtet sind.
                    Diese sind extrem nützlich und sorgen dafür,
                    dass man im Urlaub sprachlich gut zurechtkommt.
                    Alles in allem ist die Sprachschule Mäurer eine hervorragende Wahl!
                </p>
            </>,
            link: "https://maps.app.goo.gl/hRWGerMBBNuJobmq8",
        },
        {
            username: "Patrizia",
            stars: 5,
            message: <>
                <p>
                    Marc bietet mit seiner Sprachschule eine wirklich gute Möglichkeit,
                    um Japanisch mit viel Spaß und im eigenen Tempo zu lernen.
                    Ein toller Lehrer mit viel Ahnung und fairen Preisen.
                </p>
                <p>
                    Man macht schnell Fortschritte und kann innerhalb weniger Wochen
                    bereits erste Sätze sprechen. Absolute Empfehlung! :)
                </p>
            </>,
            link: "https://maps.app.goo.gl/fmZZNvc32hF4W45E8",
        },
        {
            username: "Dominic",
            stars: 5,
            message: <>
                <p>
                    Modernes Unterrichtsmaterial und ein netter Lehrer,
                    der einem immer hilfreiche Tipps gibt und von seinen
                    Erfahrungen im Ausland und mit der jeweiligen Sprache berichtet.
                </p>
                <p>Was will man mehr?</p>
                <p>
                    Der Unterricht ist locker und macht Spaß.
                    Ich jedenfalls freue mich immer darauf.
                </p>
            </>,
            link: "https://maps.app.goo.gl/VLCx8bbJngPRddnd9",
        },
        {
            username: "Nelia",
            stars: 5,
            message: <>
                <p>
                    Ich nehme seit ein paar Monaten Unterricht bei Marc,
                    er ist ein toller Lehrer, der Unterricht macht Spaß,
                    man lernt sehr gut japanisch zu sprechen und zu schreiben und
                    er kennt auch einige interessante Fakten über japanische Geschichte.
                </p>
                <p>
                    Der Unterricht ist nur weiter zu empfehlen!
                </p>
            </>,
            link: "https://maps.app.goo.gl/in39DCg1bZsJEfC18",
        },
        {
            username: "HaleStorm",
            stars: 5,
            message: <>
                <p>
                    Symphytischer Lehrer.
                </p>
                <p>
                    Es wird sich Zeit genommen um auf die Lernbedürfnisse des Schülers
                    einzugehen und den Unterricht dementsprechend zu gestallten.
                    Preis Leistung ist mehr als gut, wenn man beedenkt dass es wirklich
                    individualiserter Einzelunterricht ist und nich nach 08/15 Lehrplan
                    im Klassenraum.
                </p>
                <p>
                    5/5 kann ich wirklich nur jedem empfehlen der Überlegt japanisch zu lernen !
                </p>
            </>,
            link: "https://maps.app.goo.gl/nF5TvTwD6ohwtWFDA",
        },
        {
            username: "Pavel",
            stars: 5,
            message: <>
                <p>
                    Sehr guter und einfach zu verstehender Japanisch Unterricht.
                </p>
                <p>
                    Lehrer zudem auch sehr freundlich und humorvoll.
                </p>
                <p>
                    Lernen mit ihm macht die japanische Sprache einfach aussehen.
                </p>
                <p>
                    Kann ich jedem empfehlen.
                </p>
            </>,
            link: "https://maps.app.goo.gl/8hteyhoLtkVUthHt8",
        },
        {
            username: "Thyrit12",
            stars: 5,
            message: <>
                <p>
                    Mit Marc als Lehrer macht das Lernen immer Spaß und
                    er bringt den Unterricht gut und lebendig rüber,
                    sodass ich mich jetzt auch viel viel selbstbewusster
                    fühle wenn ich mich mit Japaner auf Japanisch unterhalte!
                </p>
                <p>
                    Top, kanns nur weiter empfehlen!!!
                </p>
            </>,
            link: "https://maps.app.goo.gl/adyQVaK1KJMzzMyd8",
        },
    ];

    useEffect(() => {
        controls.current = animate(
            "#reviews",
            { x: [0, -400 * reviews.length] },
            {
                duration: reviews.length * 15,
                repeat: Infinity,
                repeatType: "loop",
                ease: "linear",
            }
        );
        return () => controls.current.stop();
    }, []);

    // TODO instead of pause just slow down
    return (
        <div className="w-full py-10 ">
            <ScrollShadow
                orientation="horizontal"
                className="flex overflow-x-hidden max-w-full"
                onMouseEnter={() => controls.current?.pause()}
                onMouseLeave={() => controls.current?.play()}
            >
                <div id="reviews" className="flex">
                    {[...reviews, ...reviews, reviews[0]].map((review, index) => (
                        <div key={index} className="w-[400px] px-2 h-full">
                            <div className="flex flex-col gap-2 rounded-lg p-4 bg-gray-100 h-full">
                                <header className="flex gap-2 justify-between">
                                    <span className="flex gap-2 items-center">
                                        <span className="text-amber-500 font-bold text-lg">{`${review.stars}.0`}</span>
                                        <div className="flex gap-1">
                                        {[...Array(review.stars)].map((_, i) => (
                                            <span
                                                className="[&>svg]:h-[1rem] [&>svg]:w-[1rem]"
                                                key={i}
                                            >
                                                <Star />
                                            </span>
                                        ))}
                                        </div>
                                        <h1 className="font-bold">
                                            {review.username}
                                        </h1>
                                    </span>

                                    <a
                                        href={review.link}
                                        title="Öffne Rezession"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                    >
                                        <ExternalLink
                                            className="stroke-gray-400 hover:stroke-gray-600 transition"
                                        />
                                    </a>
                                </header>

                                <main
                                    className="flex flex-col gap-2"
                                >
                                    <ScrollShadow className="flex flex-col gap-2 max-h-[200px] overflow-y-scroll">
                                        {review.message}
                                    </ScrollShadow>
                                </main>
                            </div>
                        </div>
                    ))}
                </div>
            </ScrollShadow>
        </div>
    );
}
