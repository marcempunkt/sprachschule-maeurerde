import React from "react";
import UndrawFriendship from "../assets/landing/undraw_friendship.svg";

const Landing: React.FC = () => {
    return (
        <div
            id="home"
            className="relative mx-auto mb-20 mt-36 flex w-full flex-row items-center justify-center overflow-hidden max-w-[1800px]">
            <p
                className={`absolute top-12 mx-8 my-0 font-landing text-5xl text-center font-medium leading-loose tracking-wide before:absolute before:-left-10 before:-top-24 before:-z-10 before:font-quote before:text-[10rem] before:text-slate-200 before:content-['"'] max-[654px]:hidden`}>
                Die beste Zeit einen Baum anzupflanzen, war vor 20 Jahren. Die
                zweitbeste Zeit ist jetzt.
            </p>

            <div className="-z-10 [&>svg]:h-auto [&>svg]:w-full [&>svg]:min-w-[700px] [&>svg]:opacity-70">
                <UndrawFriendship />
            </div>
        </div>
    );
};

export default Landing;
