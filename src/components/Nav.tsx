import React, { useState, useEffect } from "react";
import Link from "next/link";
import { Home, Tag, User, PhoneCall } from "react-feather";
import { scrollToTop } from "../utils/scroll";
import { Navbar, NavbarBrand, NavbarContent, NavbarItem, Button,
         ButtonProps, NavbarMenuToggle, NavbarMenu, NavbarMenuItem } from "@nextui-org/react";
import Logo from "../assets/logo/logo_full_noinkscape.svg";
import { useTheme } from "next-themes";

function Nav(props: { isDataProtectionStatementPage?: boolean }) {
    const {theme, setTheme} = useTheme();

    useEffect(() => {
        setTheme("light");
    }, []);

    const [isMenuOpen, setIsMenuOpen] = useState<boolean>(false);

    // FIXME BUG fix for next ui setIsMenuOpen == false
    // to close mobile menu on click
    const [mobileMenuClicked, setMobileMenuClicked] = useState<boolean>(false);
    useEffect(() => {
        if (isMenuOpen) setIsMenuOpen(false);
    }, [mobileMenuClicked]);

    const defaultBtnProps = {
        variant: "light",
        size: "lg",
        radius: "md",
        className: "[&>a]:flex [&>a]:gap-2 [&>a]:justify-center [&>a]:items-center",
        onClick: () => setMobileMenuClicked(!mobileMenuClicked),
    } satisfies ButtonProps;

    const primaryBtnProps = {
        ...defaultBtnProps,
        variant: "solid",
        color: "primary",
    } satisfies ButtonProps;

    const secondaryBtnProps = {
        ...defaultBtnProps,
        variant: "solid",
        color: "secondary",
    } satisfies ButtonProps;

    const navItems = [
        <Button
            key="1"
            {...defaultBtnProps}
            as={Link} href="/"
            onClick={() => {scrollToTop(), setMobileMenuClicked(!mobileMenuClicked)}}
        >
            <Home /> Home
        </Button>,
        <Button
            key="1"
            {...defaultBtnProps}
            as={Link}
            href="/#offer"
        >
            <Tag />
            <span>Preise</span>
        </Button>,
        <Button
            key="1"
            {...defaultBtnProps}
            as={Link}
            href="/#aboutme"
        >
            <User />
            <span>Über&nbsp;mich</span>
        </Button>
    ];

    const calltoactions = [
        <Button
            key="1"
            {...secondaryBtnProps}
            as={Link}
            href="tel:+49152-3479-4964"
        >
            <PhoneCall />
            01523&minus;4794&minus;964
        </Button>,
        <Button
            key="1"
            {...primaryBtnProps}
            as={Link}
            href="/#contact"
        >
            <span>Jetzt&nbsp;anfangen!</span>
        </Button>
    ];

    return (
        <Navbar
            maxWidth="2xl"
            isBlurred={false}
            isBordered
            isMenuOpen={isMenuOpen}
            onMenuOpenChange={setIsMenuOpen}
        >
            <NavbarBrand>
                <Link
                    href="/"
                    className="flex items-center justify-center"
                    onClick={scrollToTop}
                    aria-label="Home"
                >
                    <Logo className="fill-slate-800 h-12 w-24 max-[200px]:h-6 max-[200px]:w-12" />
                </Link>
            </NavbarBrand>

            <NavbarContent className="flex gap-2">
                {navItems.map((item, key) => (
                    <NavbarItem key={key} className="max-[930px]:hidden">
                        {item}
                    </NavbarItem>
                ))}

                {calltoactions.map((item, key) => (
                     <NavbarItem key={key} className="max-[520px]:hidden">
                        {item}
                    </NavbarItem>
                ))}
            </NavbarContent>

            <NavbarMenu className="items-end h-fit w-full border-b-2 [&>li]:w-full [&>li>a]:w-full">
                {navItems.map((item, key) => (
                    <NavbarMenuItem key={key}>
                        {item}
                    </NavbarMenuItem>
                ))}
                {calltoactions.map((item, key) => (
                     <NavbarMenuItem key={key} className="">
                        {item}
                    </NavbarMenuItem>
                ))}
            </NavbarMenu>

            <NavbarMenuToggle
                aria-label={isMenuOpen ? "Close menu" : "Open menu"}
                className="min-[930px]:hidden"
            />
        </Navbar>
    );
};

export default Nav;
