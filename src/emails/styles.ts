export const footerLink = {
    color: "#b7b7b7",
    textDecoration: "underline",
};

export const footerLogos = {
    marginBottom: "32px",
    paddingLeft: "8px",
    paddingRight: "8px",
    width: "100%",
};

export const main = {
    backgroundColor: "#ffffff",
    margin: "0 auto",
    fontFamily:
        "-apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif",
};

export const container = {
    maxWidth: "600px",
    margin: "0 auto",
};

export const logoContainer = {
    marginTop: "32px",
};

export const h1 = {
    color: "#1d1c1d",
    fontSize: "36px",
    fontWeight: "700",
    margin: "30px 0",
    padding: "0",
    lineHeight: "42px",
};

export const heroText = {
    fontSize: "20px",
    lineHeight: "28px",
    marginBottom: "30px",
};

export const codeBox = {
    background: "rgb(245, 244, 245)",
    borderRadius: "4px",
    padding: "10px",
};
