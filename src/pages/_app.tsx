import "@/styles/globals.css";
import type { AppProps } from "next/app";
import Impressum from "../components/Impressum";
import Banner from "../components/Banner";
import { NextUIProvider } from "@nextui-org/react";
import { ThemeProvider } from "next-themes";

export default function App({ Component, pageProps }: AppProps) {
    return (
        <ThemeProvider>
            <NextUIProvider>
                <Component {...pageProps} />
                <Impressum />
                <Banner />
            </NextUIProvider>
        </ThemeProvider>
    );
}
