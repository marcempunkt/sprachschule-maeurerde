import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
    return (
        <Html lang="en" className="scroll-smooth">
            <Head>
                <meta name="description" content="Lerne Japanisch oder Koreanisch in der Nähe von Frankfurt, Langenselbold, Erlensee oder Online von zu Hause aus" />
                <meta name="keywords" content="Japanisch, Nachhilfe, Unterricht, Sprachschule, Mäurer" />
                <meta name="author" content="Marc Mäurer" />
                <meta name="theme-color" content="#000000" />
                <link rel="apple-touch-icon" href="/logo192.png" />
                <link rel="manifest" href="/manifest.json" />
                <title>Sprachschule Mäurer</title>
            </Head>
            <body>
                <Main />
                <NextScript />
            </body>
        </Html>
    );
}
