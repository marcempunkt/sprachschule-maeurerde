import DataProtectionStatementPage from "../components/pages/DataProtectionStatementPage";

export default function Datenschutz() {
  return (
      <DataProtectionStatementPage />
  );
}
