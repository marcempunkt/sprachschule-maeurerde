/* import { Inter } from "next/font/google"; */
/* const inter = Inter({ subsets: ["latin"] }); */
import JapanesePage from "../components/pages/JapanesePage";
import Reviews from "../components/Reviews";

export default function Home() {
    return (
        <>
            <JapanesePage />
            <Reviews />
        </>
    );
}
