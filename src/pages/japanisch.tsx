import JapanesePage from "../components/pages/JapanesePage";
import Reviews from "../components/Reviews";

export default function Japanisch() {
  return (
      <>
          <JapanesePage />
          <Reviews />
      </>
  );
}
