import type { Config } from "tailwindcss";
import { nextui } from "@nextui-org/react";

const config: Config = {
    content: [
        "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}",
        "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
        "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
        "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
    ],
    theme: {
        extend: {
            fontFamily: {
                quote: ["quote", "sans-serif"],
                handwriting: ["handwriting", "sans-serif"],
                landing: ["landing", "sans-serif"],
            },
        },
    },
    darkMode: "class",
    plugins: [nextui({
        defaultTheme: "light",
        themes: {
            light: {
                colors: {
                    primary: {
                        DEFAULT: "#111827", // bg-gray-900
                    },
                    secondary: {
                        DEFAULT: "#f1f5f9", // bg-slate-100
                        foreground: "black",
                    },
                },
            },
        },
    })]
};

export default config;
