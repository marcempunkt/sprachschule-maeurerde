<img src="src/assets/logo/logo_full.svg" align="right" height="100" />

### Sprachschule Mäurer

> Website for my language school where I myself teach Japanese & Korean.  
> Licensed under `MIT`

<a href="https://sprachschule-mäurer.de">https://sprachschule-mäurer.de</a>

<a href="https://sprachschule-mäurer.de">
	<img src="readme/screenshot.png" href="sprachschule-mäurer.de"/>
</a>
